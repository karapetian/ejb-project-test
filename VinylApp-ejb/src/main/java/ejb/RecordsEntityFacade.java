package ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class RecordsEntityFacade extends AbstractFacade<RecordsEntity> {
    @PersistenceContext(unitName = "VinylApp-ejbPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RecordsEntityFacade() {
        super(RecordsEntity.class);
    }
    
}
