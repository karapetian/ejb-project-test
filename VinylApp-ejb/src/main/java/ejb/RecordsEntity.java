package ejb;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class RecordsEntity implements Serializable {

    private static final long serialVersionUID = 3446900986672458925L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String recordName;

    private String recordArtist;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRecordName() {
        return recordName;
    }

    public void setRecordName(String recordName) {
        this.recordName = recordName;
    }

    public String getRecordArtist() {
        return recordArtist;
    }

    public void setRecordArtist(String recordArtist) {
        this.recordArtist = recordArtist;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        RecordsEntity that = (RecordsEntity) o;
        return id.equals(that.id) &&
                recordName.equals(that.recordName) &&
                recordArtist.equals(that.recordArtist);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, recordName, recordArtist);
    }

    @Override
    public String toString() {
        return "RecordsEntity{" +
                "id=" + id +
                ", recordName='" + recordName + '\'' +
                ", recordArtist='" + recordArtist + '\'' +
                '}';
    }
}
